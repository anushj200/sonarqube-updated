Steps to uptake SonarQube on centos 7

1) Switch to root user

     sudo su - 

2) Clone the repository from gitlab by using following command

     git clone https://gitlab.com/anushj200/sonarqube-updated.git

3) Change to sonarqube-updated folder

     cd sonarqube-updated/cookbooks

4) Run chef-solo to uptake the sonarqube cookbook

     chef-solo -c solo.rb -o sonarqube::default;
